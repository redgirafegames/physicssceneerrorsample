﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    public GameObject toClone;

    public Scene newScene;
    private PhysicsScene _physicsScene;

    private void Awake()
    {
        Physics.autoSimulation = false;
    }
    
    void Start()
    {
        var csp = new CreateSceneParameters(LocalPhysicsMode.Physics3D);
        newScene = SceneManager.CreateScene("physics", csp);
        newScene = SceneManager.GetSceneAt(1);
        _physicsScene = newScene.GetPhysicsScene();

        var clone = Instantiate(toClone);
        // Cloned color to differentiate
        for (int i = 0; i < toClone.transform.childCount; i++)
        {
            clone.transform.GetChild(i).GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        }

        SceneManager.MoveGameObjectToScene(clone, newScene);
    }

    private void FixedUpdate()
    {
        _physicsScene.Simulate(Time.fixedDeltaTime);
        Physics.Simulate(Time.fixedDeltaTime);
    }
}